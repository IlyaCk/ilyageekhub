<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>home task 08</title>
</head>
<%@page session="true" %>
<body>
<form>
<table width="1">
<tr><td></td><td></td><td><a href="/ilyaLess08/Action?action=logout">close session</a></td></tr>
<c:forEach var="branch" items = "${sessionScope}">
	<tr><td><c:out value="${branch.key}"/></td><td></td><td><a href = "/ilyaLess08/Action?action=delBranch&branchName=${branch.key}">delete all branch</a></td></tr>
	<c:forEach var="itemName" items = "${branch.value}">
		<tr><td></td><td><c:out value="${itemName}"/></td>
		<td><a href = "/ilyaLess08/Action?action=delItem&branchName=${branch.key}&itemName=${itemName}">delete current value</a></td></tr>
	</c:forEach>
</c:forEach>
<tr><td><input name="branchName"></td><td></td><td><button name="action" value="addBranch" formaction="/ilyaLess08/Action">Add branch</button></td>
</tr>
<tr><td>
<select name="chosenBranchName">
<c:forEach var="branch" items = "${sessionScope}">
	<option value="<c:out value="${branch.key}"/>"><c:out value="${branch.key}"/></option>
</c:forEach>
</select>
</td><td><input name="itemName"></td><td><button name="action" value="addItem" formaction="/ilyaLess08/Action">Add item</button></td>
</tr>
</table>
</form>
</body>
</html>