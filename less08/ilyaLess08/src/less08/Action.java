package less08;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Action
 */
public class Action extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Action() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html PUBLIC>\n<html><head><meta http-equiv=\"refresh\" content=\"1; /ilyaLess08/index.jsp\"></head><body><p>servlet...</p>");
		out.println("Press <a href = \"/ilyaLess08/index.jsp\">here</a> if you are not redirected automatically");
		try {
			HttpSession sess = request.getSession(true);
			String action = request.getParameter("action");
			String itemName = request.getParameter("itemName");
			out.println(action);
			out.println("itemName = " + itemName);
			if("logout".equalsIgnoreCase(action)) {
				sess.invalidate();
				sess = request.getSession(true);
			}
			else if("addBranch".equalsIgnoreCase(action)) {
				String branchName = request.getParameter("branchName");
				if(sess.getAttribute(branchName)==null) {
					TreeSet<String> m = new TreeSet<>();
					sess.setAttribute(branchName, m);				
				}
			} else if("addItem".equalsIgnoreCase(action)) {
				String chosenBranchName = request.getParameter("chosenBranchName");
				Object o = sess.getAttribute(chosenBranchName);
				if(o instanceof TreeSet<?>) {
					@SuppressWarnings("unchecked")
					TreeSet<String> m = (TreeSet<String>)o;
					m.add(itemName);
				}
			} else if("delItem".equals(action)) {
				String branchName = request.getParameter("branchName");				
				Object o = sess.getAttribute(branchName);
				if(o instanceof TreeSet<?>) {
					@SuppressWarnings("unchecked")
					TreeSet<String> m = (TreeSet<String>)o;
					m.remove(itemName);
				}
			} else if("delBranch".equals(action)) {
				String branchName = request.getParameter("branchName");
				sess.removeAttribute(branchName);
			}
		} catch(Exception e) {
			;
		}
		out.println("<p>servlet...</p></body></html>");
		out.close();
	}

}
