package t3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class T3 {

	private static InputStream in = null;
	private static OutputStream out = null;
	
	void performCopy(String inName, String OutName, boolean doBufferByLanguage, boolean doBufferByArray) {
		long timeBefore = System.currentTimeMillis();
		int arraySize = (doBufferByArray ? 0x1000000 : 0x10);
		try {
			if(doBufferByLanguage) {
				in = new BufferedInputStream(new FileInputStream(new File(inName)));
			} else {
				in = new FileInputStream(new File(inName));
			}
			if(doBufferByLanguage) {
				out = new BufferedOutputStream(new FileOutputStream(new File(OutName)));
			} else {
				out = new FileOutputStream(new File(OutName));
			}
			byte[] b = new byte[arraySize];
			int c = 0;
			while ((c = in.read(b)) >= 0) {
				out.write(b, 0, c);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(out != null)
					out.close();
			} catch (IOException e) {
				System.err.println("Failed to close out-file");
				System.err.println(e.getMessage());
			}
			try {
				if(in != null)
					in.close();
			} catch (IOException e) {
				System.err.println("Failed to close in-file");
				System.err.println(e.getMessage());
			}
		}
		long timeAfter = System.currentTimeMillis();
		long deltaTime = timeAfter - timeBefore; 
		System.out.println("\n\nTime for copy: " + (deltaTime / 1000) + " seconds " + (deltaTime % 1000) + " milliseconds");
	}
	
	void doAll(String[] args) {
		if(args.length < 4) {
			System.out.println("Usage: " + this.getClass().getName() + " <in-file> <out-file> <do-buffer-by-library> <do-buffer-by-array>");
			return;
		}
		performCopy(args[0], args[1], new Boolean(args[2]), new Boolean(args[3]));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		(new T3()).doAll(args);
	}

}
