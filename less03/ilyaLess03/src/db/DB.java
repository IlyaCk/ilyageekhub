package db;

//import java.sql.Driver;
import java.sql.DriverManager;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.List;
import java.util.Scanner;

//import com.mysql.jdbc.*;

public class DB {
	
	static void formattedOutput(java.sql.ResultSet result1) throws SQLException {
		
		int nCols = result1.getMetaData().getColumnCount();
		String[] colNames = new String[nCols+1];
		String[] colTypes = new String[nCols+1];
		int[] width = new int[nCols+1]; 
		for(int i=1; i<=nCols; i++) {
			colNames[i] = result1.getMetaData().getColumnName(i);
			colTypes[i] = result1.getMetaData().getColumnTypeName(i);
			width[i] = colNames[i].length();
			System.out.println(colNames[i] + " " + colTypes[i]);
		}
		
		ArrayList<String[]> mainResult = new ArrayList<>();
		String[] currRow = new String[nCols+1]; 
		while(result1.next()) {
			for(int i=1; i<=nCols; i++) {
				switch(colTypes[i]) {
				case "INTEGER":
					currRow[i] = "" + result1.getInt(i) + " (int)  ";
					break;
				default:
					try {
						currRow[i] = result1.getObject(i).toString();
					} catch (NullPointerException e) {
						currRow[i] = "(null)";
					}
				}
				width[i] = Math.max(width[i], currRow[i].length());
			}
			mainResult.add(Arrays.copyOf(currRow, currRow.length));
		}
		
		for(int i=1; i<=nCols; i++) {
			System.out.printf("%" + (width[i]+1) + "s", colNames[i]);
		}
		System.out.println();
		System.out.println();
		for(String[] curr : mainResult) {
			for(int i=1; i<=nCols; i++) {
				System.out.printf("%" + ("INTEGER".equals(colTypes[i]) ? "" : "-") + (width[i]+1) + "s", curr[i]);
			}
			System.out.println();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Enter world ``default'' to connect to default DB, any other to specify DB");
		String url;
		String dbName;
		String userName;
		String password;
		Scanner in = new Scanner(System.in);
		String _1stAns = in.nextLine();
		if("default".equalsIgnoreCase(_1stAns)) {
			url = "jdbc:mysql://localhost:3306/";
			dbName = "world";
			userName = "user1";
			password = "42";
		} else {
			System.out.print("Enter DB URL  ");
			url = in.nextLine();
			System.out.print("Enter DB name ");
			dbName = in.nextLine();
			System.out.print("Enter userName");
			userName = in.nextLine();
			System.out.print("Enter password");
			/*
			in.close();
			char[] pwd = System.console().readPassword("pwd");
			password = new String(pwd);
			*/
			password = in.nextLine();
		}

		java.sql.Connection conn = null;
		try {
			//connect to database
			String driverName = "com.mysql.jdbc.Driver";
			Class.forName(driverName);
			conn = DriverManager.getConnection(url + dbName, userName, password);
			java.sql.Statement statement = conn.createStatement();
		
			String query1 = "SELECT * FROM city";
			java.sql.ResultSet result1 = statement.executeQuery(query1);
			formattedOutput(result1);
			
			System.out.println("Enter command (``quit'' for exit)");
			String comm = in.nextLine(); 
			while(!"quit".equalsIgnoreCase(comm)) {
				try {
					if(comm.contains("DELETE") || comm.contains("INSERT") || comm.contains("UPDATE")) {
						int numChanged = statement.executeUpdate(comm);
						System.out.println("" + numChanged + " records changed");
					} else {
						java.sql.ResultSet resultNext = statement.executeQuery(comm);
						formattedOutput(resultNext);
					}
				} catch(SQLException e) {
					System.out.println("Couldn't execute SQL statement");
					System.out.println(e.getMessage());
				}
				System.out.println("Enter command (``quit'' for exit)");
				comm = in.nextLine(); 
			}

			//disconnect from database
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		in.close();



	}

}
