package t2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Scanner;

public class T2 {

	static final String[] codePageList = {"UTF8", "UTF-16", "Cp1251", "KOI8_R", "KOI8_U", "Cp866"};
	static Scanner cin;
	
	static int doChoice(String prompt) {
		int choice = -1;
		do {
			System.out.println("\nThis prog supports codepages:\n");
			for(int i=0; i<codePageList.length; i++) {
				System.out.println("\t" + i + "\t" + codePageList[i]);
			}
			System.out.print("\n" + prompt + " :  ");
			choice = cin.nextInt();
		} while (choice < 0  ||  choice >= codePageList.length);
		return choice;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		cin = new Scanner(System.in);
		System.out.print("Enter file name : ");
		Reader rdr = null;
		Writer wtr = null;
		try {
			String fname = cin.nextLine();
			int cpIn = doChoice("Select input codepage");
			int cpOut = doChoice("Select output codepage");
			String fnameOutput = fname + ".re-coded-" + codePageList[cpOut];
			rdr = new InputStreamReader(new FileInputStream(new File(fname)), codePageList[cpIn]);
			wtr = new OutputStreamWriter(new FileOutputStream(new File(fnameOutput)), codePageList[cpOut]);
			int c = 0;
			while ((c = rdr.read()) >= 0) {
				wtr.write(c);
			}
			wtr.close();
			wtr = null;
			rdr.close();
			rdr = null;
			System.out.println("re-coded text was written to " + fnameOutput);
		} catch(IOException e) {
			System.err.println(e.getLocalizedMessage());
		} finally {
			if(wtr != null)
				wtr.close();
			if(rdr != null)
				rdr.close();
		}
		

	}

}
