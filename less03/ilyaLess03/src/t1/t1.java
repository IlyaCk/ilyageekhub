package t1;

import java.io.File;
import java.io.IOException;

public class t1 {

	void printUsage() {
		System.err.println("Usage :  " + this.getClass().getName() + " ( (md | rd) dir_name  | ren old_dir_name new_dir_name )");
	}
	
	/**
	 * @param Usage : ( (md | rd) dir_name  | ren old_dir_name new_dir_name )  
	 */
	public void doAll(String[] args) {
		if(args.length < 2) {
			printUsage();
			return;
		}
		switch(args[0].toUpperCase()) {
		case "MD":
			try {
				File newDir = new File(args[1]);
				if(newDir.exists()) {
					System.err.println(newDir.getCanonicalPath() + " already exists, cannot create directory with the same name");
				} else if(newDir.mkdir()) {
					System.out.println("Directory " + newDir.getCanonicalPath() + " successfullt created");
				} else {
					System.err.println("ERROR: Couldn't create " + newDir.getCanonicalPath());
				}
			} catch(SecurityException e) {
				System.err.println("Have no access for create ``" + args[1] + "'', exiting");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case "RD":
			try {
				File toBeDelDir = new File(args[1]);
				if(!toBeDelDir.exists()) {
					System.err.println("Cannot delete unexisting directory " + toBeDelDir.getCanonicalPath());
				} else {
					boolean isDir = toBeDelDir.isDirectory();
					boolean continueInSpiteNotDir = false;
					if(!isDir) {
						System.out.println(toBeDelDir.getCanonicalPath() + " is not directory. Detele anyway? (Y/N)");
						int ans = System.in.read();
						if (Character.toUpperCase(ans) == 'Y') {
							continueInSpiteNotDir = true;
						}
					}
					if(isDir || continueInSpiteNotDir) {
						if(toBeDelDir.delete()) {
							System.out.println(toBeDelDir.getCanonicalPath() + " successfully deleted");
						} else {
							System.err.println("ERROR: Couldn't delete " + toBeDelDir.getCanonicalPath());
							// to do : for non-empty propose recursive deleting
						}
					}
				}
			} catch(SecurityException e) {
				System.err.println("Have no access for delete ``" + args[1] + "'', exiting");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case "REN":
			if(args.length < 3) {
				printUsage();
				return;
			}
			try {
				File dirOldName = new File(args[1]);
				File dirNewName = new File(args[2]);
				if(dirNewName.exists()) {
					System.err.println("Cannot rename to existing directory");
					break;
				}
				if(!dirOldName.exists()) {
					System.err.println("Cannot rename unexisting directory " + dirOldName.getCanonicalPath());
				} else if(dirOldName.renameTo(dirNewName)) {
					System.out.println("Directory " + dirOldName.getCanonicalPath() + " successfully renamed to " + dirNewName.getCanonicalPath());
				} else {
					System.err.println("ERROR: Couldn't create " + dirOldName.getCanonicalPath());
				}
			} catch(SecurityException e) {
				System.err.println("Have no access for rename, exiting");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			System.err.println("Unknown command");
			printUsage();
		}
	}
	
	/**
	 * @param Usage : ( (md | rd) dir_name  | ren old_dir_name new_dir_name )  
	 */
	public static void main(String[] args) {
		(new t1()).doAll(args);
	}
}
