package jdbc07analog;


public class Item {
	String branch;
	String item;
	
	public Item (String branch, String item) {
		this.branch = new String(branch);
		this.item = new String(item);
	}

	public Item () {
		this.branch = null;
		this.item = null;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}
	
}
