package jdbc07analog;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


public class Action extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Action() {
		super();
		dataSource = getDataSource();
		theJdbc = getJdbc();
	}

	private DriverManagerDataSource dataSource;
	private JdbcTemplate theJdbc;

	public DriverManagerDataSource getDataSource() {
		if(dataSource == null) {
			dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.setUrl("jdbc:mysql://localhost:3306/primitiveoneplain");
			dataSource.setUsername("user1");
			dataSource.setPassword("" + (17+25));
		}
		return dataSource;
	}
	
	public JdbcTemplate getJdbc() {
		if(theJdbc==null) {
			theJdbc = new JdbcTemplate(getDataSource());
		}
		return theJdbc;
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		out.println("<!DOCTYPE html PUBLIC>\n<html><head><meta http-equiv=\"refresh\" content=\"30; /ht07viaJdbcTemplate/show.jsp\"></head><body><p>servlet...</p>");
		out.println("<p>Press <a href = \"/ht07viaJdbcTemplate/show.jsp\">here</a>; automatic redirect after 30 seconds only</p>");
		try {
			HttpSession sess = req.getSession(true);
			String action = req.getParameter("action");
			String branchName = req.getParameter("branchName");				
			out.println("<p>action = " + action + "</p>");
			try {
				if("addItem".equalsIgnoreCase(action)) {
					String itemName = req.getParameter("itemName");
////				theJdbc.execute("INSERT INTO DATA\n(branch, item) VALUES ('?','?')", branchName, itemName);
					theJdbc.execute("INSERT INTO DATA\n(branch, item) VALUES ('" + branchName + "', '" + itemName + "')");
				} else if("delItem".equals(action)) {
					String itemName = req.getParameter("itemName");
					theJdbc.execute("DELETE FROM DATA\nWHERE BRANCH = '" + branchName + "' AND ITEM = '" + itemName + "'");
				} else if("delBranch".equals(action)) {
					theJdbc.execute("DELETE FROM DATA\nWHERE BRANCH = '" + branchName + "'");
				}
			} catch(DataAccessException e) {
				e.printStackTrace();
				out.println("<color=\"red\">ERROR!!! " + e.getMessage() + " DETECTED!!!<color>");
			}
			sess.setAttribute("dbPairs", theJdbc.query("SELECT * FROM DATA", new BeanPropertyRowMapper<Item>(Item.class)));
			sess.setAttribute("dbBranches", theJdbc.query("SELECT DISTINCT branch FROM DATA", new BeanPropertyRowMapper<Branch>(Branch.class)));

			out.println("<p>");
			out.println("dbPairs!");
			out.println("dbPairs = " + sess.getAttribute("dbPairs"));
			out.println("</p>");
			out.println("<p>");
			out.println("dbBranches!");
			out.println("dbBranches = " + sess.getAttribute("dbBranches"));
			out.println("</p>");
		} catch(Exception e) {
			e.printStackTrace();
		}
		out.println("<p>servlet...</p></body></html>");
		out.close();
	}
}
