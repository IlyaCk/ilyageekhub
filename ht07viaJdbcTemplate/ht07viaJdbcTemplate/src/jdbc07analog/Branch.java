package jdbc07analog;

public class Branch {
	String branch;

	public Branch () {
		this.branch = null;
	}

	public Branch (String branch) {
		this.branch = new String(branch);
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

}
