<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>home task 07 re-made with JdbcTemplate</title>
</head>
<%@page session="true" %>
<body>
<form>
<button name="action" value="refreshonly" formaction="/ht07viaJdbcTemplate/Action?action=refreshonly">ReFresh (for view others' changes)</button>
<table>
<c:forEach var="itemPair" items = "${sessionScope.dbPairs}">
	<tr><td><c:out value="${itemPair.branch}"/></td><td><c:out value="${itemPair.item}"/></td>
	<td><a href = "/ht07viaJdbcTemplate/Action?action=delItem&branchName=${itemPair.branch}&itemName=${itemPair.item}">delete this pair</a></td>
	<td><a href = "/ht07viaJdbcTemplate/Action?action=delBranch&branchName=${itemPair.branch}">delete whole branch ``<c:out value="${itemPair.branch}"/>''</a></td>
	</tr>
</c:forEach>
<tr><td>
<input name="branchName">
</td><td>
<input name="itemName">
</td><td>
<button name="action" value="addItem" formaction="/ht07viaJdbcTemplate/Action">Add (inputting branch)</button></td>
</tr>
<tr><td>
<select name="chosenBranchName">
<c:forEach var="branch" items = "${sessionScope.dbBranches}">
	<option value="<c:out value="${branch.branch}"/>"><c:out value="${branch.branch}"/></option>
</c:forEach>
</select>
</td><td><input name="itemName"></td><td><button name="action" value="addItem" formaction="/ht07viaJdbcTemplate/Action">Add (choosing branch)</button></td>
</tr>
</table>
</form>
</body>
</html>