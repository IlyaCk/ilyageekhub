

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MessageWithForms
 * 
 * Rather poor exception handling and debug messages, 
 * because data are usually got from the servlet itself
 */
public class MessageWithForms extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	String cutTooLong(String s) {
		if(s.length() < 0x100) {
			return s;
		}
		return s.substring(0, 0xF0) + " ... (totally " + s.length() + " characters)";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * Just now, assume that the same name in servlet context and session context 
	 * are fully independent, can be changed separately, and so on
	 *  
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>\n<head><title>GeekHub hometask 07 (\"messages\"), including forms</title></head>\n<body>");
		HttpSession sess = request.getSession(true);
		ServletContext servCont = request.getServletContext();
		try {
			out.println("<p>Current time is: " + Calendar.getInstance().getTime().toString() + "</p>");
			String messageParam = request.getParameter("message");
			out.println("<font size = 7>message = " + messageParam + "</font>");			
			String scopeParam = request.getParameter("scope");
			boolean isGlobalScope = "app".equals(scopeParam);
			String actionParam = request.getParameter("action");
			if("logout".equalsIgnoreCase(actionParam)) {
				sess.invalidate();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					;
				}
				out.println("<p>Invalidate (logout) performed</p>");
				sess = request.getSession();
			} else {
				String nameParam = request.getParameter("name");
				if(nameParam.isEmpty()) {
					throw new NullPointerException();
				}
				if("add".equalsIgnoreCase(actionParam) || "update".equalsIgnoreCase(actionParam)) {
					String valueParam = request.getParameter("value");
					Object o = isGlobalScope ? 
							servCont.getAttribute(nameParam) :
							sess.getAttribute(nameParam);
					Store currStore = (o == null || !(o instanceof Store)) ? null : (Store)o;
					if(currStore==null && "update".equalsIgnoreCase(actionParam)) {
						out.println("<p>WARNING! updating of non-existing attribute actually mean adding</p>");
					} else if(currStore==null && "update".equalsIgnoreCase(actionParam)) {
						out.println("<p>WARNING! adding of existing attribute actually mean updating</p>");
					}
					if(currStore==null) {
						if(isGlobalScope) 
							servCont.setAttribute(nameParam, new Store(valueParam));
						else
							sess.setAttribute(nameParam, new Store(valueParam));
					} else {
						currStore.update(valueParam);
					}
				} else if("remove".equalsIgnoreCase(actionParam)) {
					if(isGlobalScope) {
						servCont.removeAttribute(nameParam);
					} else {
						sess.removeAttribute(nameParam);
					}
				} 
			}
		} catch (NullPointerException e) {
			out.println("<p>WARNING! some parameters were wrong, report to site administrators</p>");
		}
//		out.println("<form autocomplete = \"on\"  action = \"./mess-forms\">");
//		out.println("<form autocomplete = \"on\"  action = \"./MessageWithForms\">");
		out.println("<form autocomplete = \"on\"  action = \"/ilyaLess07/mess-forms\">");
		out.println("<p>message <input name = \"message\"></p>");
		out.println("<p><table><tr><td>action:</td>");
		out.println("<td><input type = \"radio\" name = \"action\" value = \"add\" checked = \"true\">add<Br></td></tr>");
		out.println("<tr><td></td><td><input type = \"radio\" name = \"action\" value = \"update\">update<Br></td></tr>");
		out.println("<tr><td></td><td><input type = \"radio\" name = \"action\" value = \"remove\">remove<Br></td></tr>");
		out.println("<tr><td></td><td><input type = \"radio\" name = \"action\" value = \"logout\">logout</td></tr>");
		out.println("</table></p>");
		out.println("<p><table><tr><td>scope:</td>");
		out.println("<td><input type = \"radio\" name = \"scope\" value = \"session\" checked = \"true\">session<Br></td></tr>");
		out.println("<tr><td></td><td><input type = \"radio\" name = \"scope\" value = \"app\">app<Br></td></tr>");
		out.println("</table></p>");
		out.println("<p>name <input name = \"name\"></p>");
		out.println("<p>value <input name = \"value\"></p>");
		out.println("<input type = \"submit\" text = \"Apply\">");
		out.println("</form>");
		
		Enumeration<String> allAttribs;
		allAttribs = (sess==null) ? null : sess.getAttributeNames();
		if(allAttribs!=null && allAttribs.hasMoreElements()) {
			out.println("<h1>Session-context attributes:</h1>");
			out.println("<table border=\"2\">");
			while(allAttribs.hasMoreElements()) {
				String attrName = allAttribs.nextElement();
				out.print("<tr><td> " + attrName + "\t </td><td> ");
				try {
					Object o = sess.getAttribute(attrName);
					if(o!=null && (o instanceof Store)) {
						Store attrVal = (Store)o;
						out.print(cutTooLong(attrVal.getData().toString()) + "\t </td><td> " + attrVal.getCreated().toString() + "\t </td><td> " + attrVal.getModified().toString() + "\t </td><td> ");  
					} else {
						out.print(cutTooLong(o.toString()) + "\t </td><td> cannot determine\t </td><td> cannot determine\t </td><td> ");				
					}
					if(servCont!=null && servCont.getAttribute(attrName) != null) {
						out.print("same-name attribute exists in servlet conext");
					}
				} catch(NullPointerException e) {
					out.print("NullPointerException catched");
				}
				out.println("</td></tr>");
			}
			out.println("</table>");
		} else {
			out.println("<h1>Session-context attributes &mdash; none</h1>");
		}
		
		allAttribs = (servCont==null) ? null : servCont.getAttributeNames();
		if(allAttribs.hasMoreElements()) {
			out.println("<h1>Servlet-context attributes:</h1>");
			out.println("<table border=\"2\">");
			while(allAttribs.hasMoreElements()) {
				String attrName = allAttribs.nextElement();
				out.print("<tr><td> " + attrName + "\t </td><td> ");
				Object o = servCont.getAttribute(attrName);
				if(o instanceof Store) {
					Store attrVal = (Store)o;
					out.print(cutTooLong(attrVal.getData().toString()) + "\t </td><td> " + attrVal.getCreated().toString() + "\t </td><td> " + attrVal.getModified().toString() + "\t </td><td> ");  
				} else {
					out.print(cutTooLong(o.toString()) + "\t </td><td> cannot determine\t </td><td> cannot determine\t </td><td> ");				
				}
				if(sess!=null && sess.getAttribute(attrName) != null) {
					out.print("same-name attribute exists in session conext");
				}
				out.println("</td></tr>");
			}
			out.println("</table>");
		} else {
			out.println("<h1>Servlet-context attributes &mdash; none</h1>");
		}

		out.println("</html>");
		out.close();
	}

}
