import java.util.Calendar;
import java.util.Date;


class Store {
	String data;
	Date created, modified;
	Store(String d_) {
		data = d_;
		created = Calendar.getInstance().getTime();
		modified = created;
	}
	void update(String d_) {
		data = d_;
		modified = Calendar.getInstance().getTime();
	}
	String getData() {
		return data; 
	}
	Date getCreated() {
		return created;
	}
	Date getModified() {
		return modified;
	}
}