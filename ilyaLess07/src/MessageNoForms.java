

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MessageNoForms
 */
public class MessageNoForms extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public MessageNoForms() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * Just now, assume that the same name in servlet context and session context 
	 * are fully independent, can be changed separately, and so on   
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>\n<head><title>GeekHub hometask 07 (\"messages\"), no forms</title></head>\n<body>");
		try {
			String messageParam = request.getParameter("message");
			if(messageParam.isEmpty()) {
				throw new NullPointerException();
			}
			out.println("<font size = 7>message = " + messageParam + "</font>");			
		} catch (NullPointerException e) {
			out.println("<p>WARNING! No \"message\" param given</p>");
		}
		boolean isGlobalScope = false;
		try {
			String scopeParam = request.getParameter("scope");
			if("app".equalsIgnoreCase(scopeParam) || "global".equalsIgnoreCase(scopeParam)) {
				isGlobalScope = true;
			} else if(!("local".equalsIgnoreCase(scopeParam) || "session".equalsIgnoreCase(scopeParam) || "sesion".equalsIgnoreCase(scopeParam))) {
				out.println("<p>WARNING: cannot understand \"scope\" value, assuming scope = session</p>");
			}
		} catch(NullPointerException e) {
			out.println("<p>WARNING: no \"scope\" param given, assuming scope = session</p>");
		}
		HttpSession sess = request.getSession(true);
		ServletContext servCont = request.getServletContext();
		try {
			String actionParam = request.getParameter("action");
			if(actionParam.isEmpty()) {
				throw new NullPointerException();
			}
			if("invalidate".equalsIgnoreCase(actionParam) || "logout".equalsIgnoreCase(actionParam)) {
				sess.invalidate();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					;
				}
				out.println("<p>Invalidate (logout) performed</p>");
				sess = request.getSession();
			} else {
				try {
					String nameParam = request.getParameter("name");
					if(nameParam.isEmpty()) {
						throw new NullPointerException();
					}
					if("add".equalsIgnoreCase(actionParam) || "update".equalsIgnoreCase(actionParam)) {
						try {
							String valueParam = request.getParameter("value");
							Object o = isGlobalScope ? 
									(servCont==null ? null : servCont.getAttribute(nameParam)) : 
									(sess==null ? null : sess.getAttribute(nameParam));
							Store currStore = (o == null || !(o instanceof Store)) ? null : (Store)o;
							if(currStore==null && "update".equalsIgnoreCase(actionParam)) {
								out.println("<p>WARNING! updating of non-existing attribute actually mean adding</p>");
							} else if(currStore==null && "update".equalsIgnoreCase(actionParam)) {
								out.println("<p>WARNING! adding of existing attribute actually mean updating</p>");
							}
							if(currStore==null) {
								if(isGlobalScope) 
									servCont.setAttribute(nameParam, new Store(valueParam));
								else
									sess.setAttribute(nameParam, new Store(valueParam));
							} else {
								currStore.update(valueParam);
							}
						} catch (NullPointerException e) {
							out.println("<p>WARNING! No \"value\" param given, action \"" + actionParam + "\" is not performed</p>");
						}
					} else if("erase".equalsIgnoreCase(actionParam) || "del".equalsIgnoreCase(actionParam) || "delete".equalsIgnoreCase(actionParam) || "erase".equalsIgnoreCase(actionParam)) {
						if(isGlobalScope) {
							servCont.removeAttribute(nameParam);
						} else {
							sess.removeAttribute(nameParam);
						}
					} else {
						out.println("<p>Incorrect \"action\" param. Possible values are:</p>\n" +
								"<p>&nbsp&nbsp&nbsp&nbsp\"add\",</p>" +
								"<p>&nbsp&nbsp&nbsp&nbsp\"update\",</p>" +
								"<p>&nbsp&nbsp&nbsp&nbsp\"remove\",</p>" +
								"<p>&nbsp&nbsp&nbsp&nbsp\"logout\"</p>");
					}
				} catch (NullPointerException e) {
					out.println("<p>WARNING! No \"name\" param given, action \"" + actionParam + "\" is not performed</p>");
				}
			}
		} catch (NullPointerException e) {
			out.println("<p>WARNING! No \"action\" param given</p>");
		}
		Enumeration<String> allAttribs;
		
		allAttribs = (sess==null) ? null : sess.getAttributeNames();
		if(allAttribs!=null && allAttribs.hasMoreElements()) {
			out.println("<h1>Session-context attributes:</h1>");
			out.println("<table border=\"2\">");
			while(allAttribs.hasMoreElements()) {
				String attrName = allAttribs.nextElement();
				out.print("<tr><td> " + attrName + "\t </td><td> ");
				try {
					Object o = sess.getAttribute(attrName);
					if(o!=null && (o instanceof Store)) {
						Store attrVal = (Store)o;
						out.print(attrVal.getData().toString() + "\t </td><td> " + attrVal.getCreated().toString() + "\t </td><td> " + attrVal.getModified().toString() + "\t </td><td> ");  
					} else {
						out.print(o.toString() + "\t </td><td> cannot determine\t </td><td> cannot determine\t </td><td> ");				
					}
					if(servCont!=null && servCont.getAttribute(attrName) != null) {
						out.print("same-name attribute exists in servlet conext");
					}
				} catch(NullPointerException e) {
					out.print("NullPointerException catched");
				}
				out.println("</td></tr>");
			}
			out.println("</table>");
		} else {
			out.println("<h1>Session-context attributes &mdash; none</h1>");
		}
		
		allAttribs = (servCont==null) ? null : servCont.getAttributeNames();
		if(allAttribs.hasMoreElements()) {
			out.println("<h1>Servlet-context attributes:</h1>");
			out.println("<table border=\"2\">");
			while(allAttribs.hasMoreElements()) {
				String attrName = allAttribs.nextElement();
				out.print("<tr><td> " + attrName + "\t </td><td> ");
				Object o = servCont.getAttribute(attrName);
				if(o instanceof Store) {
					Store attrVal = (Store)o;
					out.print(attrVal.getData().toString() + "\t </td><td> " + attrVal.getCreated().toString() + "\t </td><td> " + attrVal.getModified().toString() + "\t </td><td> ");  
				} else {
					out.print(o.toString() + "\t </td><td> cannot determine\t </td><td> cannot determine\t </td><td> ");				
				}
				if(sess!=null && sess.getAttribute(attrName) != null) {
					out.print("same-name attribute exists in session conext");
				}
				out.println("</td></tr>");
			}
			out.println("</table>");
		} else {
			out.println("<h1>Servlet-context attributes &mdash; none</h1>");
		}
		out.println("</html>");
		out.close();
	}
}
