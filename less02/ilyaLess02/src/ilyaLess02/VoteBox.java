package ilyaLess02;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class VoteBox implements Comparable<VoteBox>, Collection<Vote> {
	List<Vote> allVotes;
	final String nameToReject;
	final String nameToDuplicate;
	int wantedVotes;
	Map<String,AtomicInteger> results;
	
	public VoteBox(String r, String d) {
		allVotes = new ArrayList<>();
		nameToReject = new String(r);
		nameToDuplicate = new String(d);
		wantedVotes = 0;
		results = new HashMap<>();
	}
	
	public boolean add(Vote v) {
		if(v.lookSelectedName().equals(nameToReject)) {
			return false;
		}
		if(v.lookSelectedName().equals(nameToDuplicate)) {
			allVotes.add(v); // one time here, another time as usual
			if(results.containsKey(v.lookSelectedName())) {
				results.get(v.lookSelectedName()).addAndGet(1);
			} else {
				results.put(v.lookSelectedName(), new AtomicInteger(1));
			}
			wantedVotes++;
		}
		allVotes.add(v);
		if(results.containsKey(v.lookSelectedName())) {
			results.get(v.lookSelectedName()).addAndGet(1);
		} else {
			results.put(v.lookSelectedName(), new AtomicInteger(1));
		}
		return true; // ??? ??? ???
	}

	@Override
	public int compareTo(VoteBox that) {
		if(this.wantedVotes > that.wantedVotes)
			return +1;
		if(this.wantedVotes < that.wantedVotes)
			return -1;
		int thisSize = this.allVotes.size();
		int thatSize = that.allVotes.size();
		if(thisSize < thatSize)
			return +1;
		if(thisSize > thatSize)
			return -1;
		for(int i=0; i<thisSize; i++) {
			int currCmp = this.allVotes.get(i).lookSelectedName().compareTo(that.allVotes.get(i).lookSelectedName());
			if(currCmp != 0)
				return currCmp;
		}
		return 0;
	}
	
	@Override
	public String toString() {
		return results.toString() + "\n" + allVotes.toString();		
	}

	@Override
	public boolean addAll(Collection<? extends Vote> vC) {
		boolean result = true;
		for(Vote v : vC) {
			if(!this.add(v)) {
				result = false;
			}
		}
		return result;
	}

	@Override
	public void clear() {
		allVotes.clear();
		results.clear();
		wantedVotes = 0;
	}

	public boolean contains(Object v) {
		return allVotes.contains(v);
	}

	public boolean containsAll(Collection<?> cV) {
		return allVotes.containsAll(cV);
	}

	@Override
	public boolean isEmpty() {
		return allVotes.isEmpty();
	}

	@Override
	public Iterator<Vote> iterator() {
		return allVotes.iterator();
	}

	@Override
	public boolean remove(Object arg0) {
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		return false;
	}

	public boolean retainAll(Collection<?> arg0) {
		return false;
	}

	@Override
	public int size() {
		return allVotes.size();
	}

	public Object[] toArray() {
		return allVotes.toArray();
	}

	public <T> T[] toArray(T[] arg0) {
		return allVotes.toArray(arg0);
	}

}
