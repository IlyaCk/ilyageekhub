package ilyaLess02;

import java.util.Random;

public class CatLauncher {

	public static void main(String[] args) {
		Random r = new Random();
		int comparesPerformed = 0;
		int collisionsFound = 0;
		while(true) {
			int[] col = {r.nextInt(0x100), r.nextInt(0x100), r.nextInt(0x100)}; 
			Cat catOne = new CatOrthodoxEquals(col, r.nextInt(240));
			int[] colTwo = {r.nextInt(0x100), r.nextInt(0x100), r.nextInt(0x100)};
			Cat catTwo = new CatHashEquals(colTwo, r.nextInt(240));
			comparesPerformed++;
			if(catOne.equals(catTwo) != catTwo.equals(catOne)) {
				collisionsFound++;
				System.out.println("" + comparesPerformed + " compares performed, " + collisionsFound + " collisions found");
			} else if(comparesPerformed % 1000000 == 0) {
				System.out.println("" + comparesPerformed + " compares performed, " + collisionsFound + " collisions found");
			}
		}
	}
}
