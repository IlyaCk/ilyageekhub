package ilyaLess02;

public class VoteBoxLauncher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		VoteBox theVB1 = new VoteBox("A", "B");
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("C"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("D"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("C"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		System.out.println(theVB1);

		VoteBox theVB2 = new VoteBox("B", "D");
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("B"));
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("C"));
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("B"));
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("D"));
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("B"));
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("C"));
		theVB2.add(new Vote("A"));
		theVB2.add(new Vote("B"));
		theVB2.add(new Vote("A"));
		System.out.println(theVB2);
	}

}
