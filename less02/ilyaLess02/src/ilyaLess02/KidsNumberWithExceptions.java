package ilyaLess02;

/**
 * 
 * @author ilya
 *
 * This class can store numbers for kids --
 * natural numbers in range [1..9]. 
 * 
 *   
 */
public class KidsNumberWithExceptions {
	protected int value;
	
	KidsNumberWithExceptions(int v) {
		value = Math.max(1,Math.min(9,v));		
	}
	
	KidsNumberWithExceptions() {
		value = 1;		
	}
	
	/**
	 * Presents number as usual decimal digit and as quantity of sticks
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<value; i++) {
			sb.append('|');
		}
		return "(" + value + " is so much: " + sb.toString() + ")";
	}
	
	KidsNumberWithExceptions add(KidsNumberWithExceptions that) throws ExTooMany {
		if(this.value + that.value > 9)
			throw new ExTooMany("sum of " + this.toString() + " and " + that.toString() + " is too large for kids");
		return new KidsNumberWithExceptions(this.value + that.value);
	}

	KidsNumberWithExceptions subtract(KidsNumberWithExceptions that) {
		if(this.value <= that.value)
			throw new ExRTTooSmall("kids cannot subtract larger number " + that.toString() + " from smaller number " + this.toString());
		return new KidsNumberWithExceptions(this.value - that.value);
	}

}
