package ilyaLess02;

public class ExLauncher {

	static protected KidsNumberWithExceptions showSum(KidsNumberWithExceptions a, KidsNumberWithExceptions b) {
		try {
			KidsNumberWithExceptions res = a.add(b);
			System.out.println("sum of " + a + " and " + b + " is " + res);
			return res; 			
		} catch(ExTooMany e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public static void main(String[] args) {
		KidsNumberWithExceptions two = new KidsNumberWithExceptions(2);
		KidsNumberWithExceptions five = new KidsNumberWithExceptions(5);
		
		KidsNumberWithExceptions seven = showSum(two, five);
		
		KidsNumberWithExceptions twelve = showSum(five, seven);
		System.out.println(twelve);
		
		try {
			KidsNumberWithExceptions minusThree = two.subtract(five);
			System.out.println(minusThree);
		} catch(ExRTTooSmall e) {
			System.out.println(e.getMessage());
		}
		
		KidsNumberWithExceptions minusThreeWithoutCatches = two.subtract(five);
		System.out.println(minusThreeWithoutCatches);
		
		
		

	}

}
