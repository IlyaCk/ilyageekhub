package ilyaLess02;

/**
 * Generate exception, assuming that kids don't know 
 * neither negative numbers nor zero 
 */
public class ExRTTooSmall extends RuntimeException {

	private static final long serialVersionUID = 5343792654124942907L;
	
	public ExRTTooSmall() {
		
	}
	
	public ExRTTooSmall(String s) {
		super(s);
	}	
	
}
