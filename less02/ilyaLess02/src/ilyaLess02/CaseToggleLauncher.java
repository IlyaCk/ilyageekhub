package ilyaLess02;

import ilyaLess02.CaseToggler;

public class CaseToggleLauncher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length==0) {
			System.err.println("A string should be passeds as cmd-line param");
			return;
		}
		System.out.println(CaseToggler.caseToggle(args[0]));
		System.out.println("\n\nSome more examples:");
		System.out.println(CaseToggler.caseToggle("hello"));
		System.out.println(CaseToggler.caseToggle("Hello"));
		System.out.println(CaseToggler.caseToggle("42"));
	}

}
