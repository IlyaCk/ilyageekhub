package ilyaLess02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Garage {

	public static void main(String[] args) {
		List<Car> allCars = new ArrayList<>();
		allCars.add(new Car("VAZ-2101", "white", 2424));
		allCars.add(new Car("VAZ-2101", "safari", 2424));
		allCars.add(new Car("VAZ-2101", "red", 2424));
		allCars.add(new Car("VAZ-2101", "blue", 2424));
		allCars.add(new Car("Gazel", "white", 5500));
		allCars.add(new Car("Gazel", "white", 2900));
		allCars.add(new Car("Mercedes", "white", 3040));
		allCars.add(new Car("Mercedes", "black", 3040));
		Collections.sort(allCars);
		System.out.println(Arrays.toString(allCars.toArray()));
	}

}
