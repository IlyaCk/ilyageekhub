package ilyaLess02;

public class CaseToggler {
	public static String caseToggle(String s) {
		if(s.length()==0)
			return s;
		StringBuilder res = new StringBuilder();
		char c = s.charAt(0);
		if(Character.isLowerCase(c)) {
			res.append(Character.toUpperCase(c));
		} else if(Character.isUpperCase(c)) {
			res.append(Character.toLowerCase(c));
		} else {
			res.append(c);
		}
		res.append(s.substring(1).toCharArray());
		return res.toString();
	}

}
