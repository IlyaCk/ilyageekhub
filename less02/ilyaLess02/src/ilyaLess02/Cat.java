package ilyaLess02;

import java.util.Arrays;

public abstract class Cat {
	int[] rgbColor;
	int age;
	
	public int[] getRGB() {
		return Arrays.copyOf(rgbColor, 3); 
	}
	
	public int getAge() {
		return age;
	}
	
	public void applyBirthDay() {
		age++;
	}
			
	public Cat(int[] color, int age) {
		if(color.length != 3) {
			throw new IllegalArgumentException("RBG[] should contain exactly 3 ints");
		}
		if(age < 0) {
			throw new IllegalArgumentException("age cannot be negative"); 
		}
		this.rgbColor = Arrays.copyOf(color, 3);
		this.age = age;
	}
	
	public abstract boolean equals(Object o); 
	
	@Override
	public String toString() {
		return "(RGB = " + Arrays.toString(rgbColor) + ", age = " + age + ")";
	}
	
	@Override
	public int hashCode() {
		return (int)((long)rgbColor.hashCode() * 42 + age);
//		int res = rgbColor[0] + rgbColor[1] + rgbColor[2] + age;
//		System.out.println("Cat : " + this.toString() + " ;  hashCode = " + res);
//		return res;
	}

}
