package ilyaLess02;

import java.util.Scanner;

public class MyStackLauncher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MyStack<Integer> theStack = new MyStack<>();
		System.out.println("Enter some strictly positive numbers, 0 for end");
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		while(n > 0) {
			theStack.add(n);
			n = in.nextInt();
		}
		in.close();
		System.out.println("Numbers were successfully pushed to MyStack LIFO");
		System.out.print("The numbers (in reverse order) are:");
		while(!theStack.isEmpty()) {
			System.out.print(" " + theStack.poll());
		}
		System.out.println();
	}

}
