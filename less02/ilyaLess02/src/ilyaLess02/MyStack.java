package ilyaLess02;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * 
 * @author ilya
 *
 * @param <T>
 * 
 * Implements stack datastructure (LIFO).
 * Actually uses LinkedList, 
 * but counts size manually 
 * (avoids using list's size() 
 * implementation assuming it may be too slow). 
 */
public class MyStack<T> implements Queue<T> {
	private List<T> lst;
	private int theSize;
	
	public MyStack() {
		lst = new LinkedList<>();
		theSize = 0;
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		try {
			for(T e : arg0) {
				this.add(e);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void clear() {
		lst.clear();
		theSize = 0;
	}

	/**
	 * this implementation actually does not perform "contains" action 
	 */
	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * this implementation actually does not perform "containsAll" action 
	 */
	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lst.isEmpty();
	}

	/**
	 * this implementation actually does not allow iterator usage 
	 */
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * this implementation actually does not perform "remove" action 
	 */
	@Override
	public boolean remove(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * this implementation actually does not perform "removeAll" action 
	 */
	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * this implementation actually does not perform "retainAll" action 
	 */
	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		return theSize;
	}

	/**
	 * this implementation actually does not perform "toArray" action 
	 */
	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * this implementation actually does not perform "remove" action 
	 */
	@Override
	public <ET> ET[] toArray(ET[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(T e) {
		try {
			lst.add(0, e);
			theSize++;
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@Override
	public T element() {
		return lst.get(0);
	}

	@Override
	public boolean offer(T e) {
		this.add(e);
		return false;
	}

	@Override
	public T peek() {
		if(lst.isEmpty())
			return null;
		return lst.get(0);
	}

	@Override
	public T poll() {
		if(lst.isEmpty())
			return null;
		T res = lst.get(0);
		theSize--;
		lst.remove(0);
		return res;
	}

	@Override
	public T remove() {
		try {
			T res = lst.get(0);
			theSize--;
			lst.remove(0);
			return res;
		} catch (Exception ex) {
			throw new NoSuchElementException(); 			
		}
	}

}
