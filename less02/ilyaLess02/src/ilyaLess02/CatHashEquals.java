package ilyaLess02;

public class CatHashEquals extends Cat {

	public CatHashEquals(int[] color, int age) {
		super(color, age);
		// TODO Auto-generated constructor stub
	}
	
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		if(!(o instanceof Cat)) {
			return false;
		}
		Cat that = (Cat)o;
//		System.out.println("applying comparing by hashcodes");
		return this.hashCode() == that.hashCode(); 
	}
	

}
