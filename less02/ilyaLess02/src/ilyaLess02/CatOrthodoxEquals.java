package ilyaLess02;

public class CatOrthodoxEquals extends Cat {

	public CatOrthodoxEquals(int[] color, int age) {
		super(color, age);
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		if(!(o instanceof Cat)) {
			return false;
		}
		Cat that = (Cat)o; 
		if(this.age != that.age) {
			return false;
		}
		if(this.rgbColor.length != that.rgbColor.length) {
			return false;
		}
		for(int i=0; i<this.rgbColor.length; i++)
			if(this.rgbColor[i] != that.rgbColor[i])
				return false;
		return false;
	}

}
