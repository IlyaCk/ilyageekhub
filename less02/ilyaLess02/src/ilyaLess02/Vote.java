package ilyaLess02;

public class Vote {
	static int totalVotes = 0;
	
	protected String selectedName;
	protected int id;
	
	public Vote(String selN) {
		id = totalVotes;
//		selectedName = new String (selN);
		selectedName = selN; // ??? !!! ???		
		totalVotes++;
	}
	
	public String lookSelectedName() {
		return selectedName;
	}
	
	@Override
	public String toString() {
		return "(id:" + id + ", selected:" + selectedName + ")";
	}

}
