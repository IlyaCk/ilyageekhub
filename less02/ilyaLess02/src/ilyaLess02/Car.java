package ilyaLess02;

public final class Car implements Comparable<Car> {
	private String brand;
	private String color;
	private int base;
	
	public Car(String givenBrand, String givenColor, int givenBase) {
		brand = givenBrand;
		color = givenColor;
		base = givenBase;
	}

	public String getBrand() {
		return brand;
	}

	public String getColor() {
		return color;
	}
	
	public int getBase() {
		return base;
	}

	@Override
	public int compareTo(Car that) {
		if(!this.color.equals(that.color))
			return this.color.compareTo(that.color);
		if(!this.brand.equals(that.brand))
			return this.brand.compareTo(that.brand);
		if(this.base < that.base)
			return +1;
		if(this.base > that.base)
			return -1;
		return 0;
	}
	
	@Override
	public String toString() {
		return "  " + this.brand + " (" + this.color + ", " + this.base + "mm) ";		
	}
	
	public String deepToString() {
		return this.toString();
	}
	
}
