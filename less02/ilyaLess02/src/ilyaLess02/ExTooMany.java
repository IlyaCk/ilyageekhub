package ilyaLess02;

/**
 * Generate exception, assuming that any number greater than 9 
 * is too large for kids  
 */
public class ExTooMany extends Exception {
	private static final long serialVersionUID = -2605219723402494559L;

	public ExTooMany() {
	}
	
	public ExTooMany(String s) {
		super(s);		
	}
}
