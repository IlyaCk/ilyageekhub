package Thr;

import java.io.File;
import java.io.IOException;

public class SearchNoThreadsOne {
	long theMatchNumber;
	String pattAsRegExp;

	void searchFrom(File dir) {
		String[] lstAll = dir.list();
		if(lstAll == null) {
			return;
		}
		for(String fname : lstAll) {
			File fnext = new File(dir, fname);
			if(fnext.isDirectory()) {
				searchFrom(fnext);
			} else {
				try {
					if(fnext.getCanonicalPath().matches(pattAsRegExp)) {
						theMatchNumber++;
						System.out.println("match " + theMatchNumber + " : " + fnext.getCanonicalPath());
					} 
				} catch (IOException e) {
					System.err.println("exception in trying to check " + fname + " inside " + dir.getAbsolutePath());
					e.printStackTrace();
				}
			}
		}
	}

	public void doAll(String[] args) {
		if(args.length < 1) {
			System.err.println("Usage : " + this.getClass().getCanonicalName() + " <pattern to search>");
			return;
		}
		String patt = args[0];
		theMatchNumber = 0;
		if(patt.contains(".")) {
			pattAsRegExp = ".*\\\\" + patt.replaceAll("\\.", "\\\\.").replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\.]*";
		} else {
			pattAsRegExp = ".*" + patt.replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\]*";
		}
		System.out.println("patt = " + patt);
		System.out.println("pattAsRegExp = " + pattAsRegExp);
		long timeZero = System.currentTimeMillis();
		File startDir = new File(".");
		searchFrom(startDir);
		long timeAfter = System.currentTimeMillis();
		System.out.println(timeAfter - timeZero);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		(new SearchNoThreadsTwo()).doAll(args);
	}

}
