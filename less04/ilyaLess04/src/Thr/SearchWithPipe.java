package Thr;

import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;


public class SearchWithPipe {
	class GenAllPaths implements Runnable {
		PipedOutputStream pipedOut;
		
		public GenAllPaths(PipedOutputStream p) {
			pipedOut = p;
		}
		
		void searchFrom(File dir, PipedOutputStream pipeOut) {
//			String[] lstAll = null;
//			try {
//				lstAll = dir.list();
//				System.out.println("searchFrom just entered to " + dir.getCanonicalPath());
//				System.out.println("lstAll == null without exception");
//			} catch (IOException e) {
//				System.err.println("Have no privilages to enter to " + dir.toString());
//				return;
//			}
			String[] lstAll = dir.list();
			if(lstAll == null) {
				return;
			}
			for(String fname : lstAll) {
				File fnext = new File(dir, fname);
//				System.out.println(fname);
				if(fnext.isDirectory()) {
					searchFrom(fnext, pipeOut);
				} else {
					try {
//						System.out.println(fnext.getCanonicalPath() + " is not directory, writing it to pipe");
						pipeOut.write((fnext.getCanonicalPath() + "\n").getBytes());
					} catch (IOException e) {
						System.err.println("exception in passing " + fname + " to pipe");
						e.printStackTrace();
					}
				}
			}
			try {
				pipeOut.flush();
//				System.out.println("pipe flushed successfully");
			} catch (IOException e) {
				System.err.println("Couldn't flush pipe");
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			File startDir = new File(".");
			searchFrom(startDir, pipedOut);
//			try {
//				pipedOut.write('�');
////				pipedOut.write(0x001a);
////				System.out.println("Successfully wrote EOF mark to pipe");
//			} catch (IOException e) {
//				System.err.println("Writing EOF mark to pipe failed");
//				e.printStackTrace();
//			}
////////			
//			try {
//				pipedOut.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}			
		}
	}
	class ChooseResults implements Runnable {
		long theMatchNumber;
		PipedInputStream in;
		PrintStream out;
		String pattAsRegExp;
		Thread genner;
		
		public ChooseResults(PipedInputStream p, PrintStream out_, String patt, Thread g) {
			theMatchNumber = 0;
			in = p;
			this.out = out_;
			genner = g;
			if(patt.contains(".")) {
				pattAsRegExp = ".*\\\\" + patt.replaceAll("\\.", "\\\\.").replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\.]*";
			} else {
				pattAsRegExp = ".*" + patt.replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\]*";
			}
			System.out.println("patt = " + patt);
			System.out.println("pattAsRegExp = " + pattAsRegExp);
		}
		
		public void applyCheck(String fname) {
			if(fname.matches(pattAsRegExp)) {
				theMatchNumber++;
				out.println("match " + theMatchNumber + " : " + fname);
			} else {
//				System.out.println(fname + " was rejected as non-matching");
			}
		}

	
		@Override
		public void run() {
			byte[] myBuffer = new byte[0x1040];
			StringBuilder sb = new StringBuilder();
			boolean endMarkerFound = false;
			while(!endMarkerFound) {
				int rr;
				try {
					rr = in.read(myBuffer, 0, 0x1000);
				} catch (NullPointerException | IndexOutOfBoundsException e) {
					System.err.println("Incorrect work of pipe's read");
					e.printStackTrace();
					rr = -1;
				} catch (IOException e) {
					System.err.println("Can't read from pipe");
					try {
						Thread.sleep(500);
						System.err.println("sleep really applied");
						if(genner.isAlive()) {
							rr = in.read(myBuffer);
							System.err.println("After pause, could read from pipe");
						} else {
							rr = -1;
							System.out.println("it's just because genner already stopped");
						}
					} catch (IOException | InterruptedException eee) {
						System.err.println("Finally Can't read from pipe");
						return;
					}
				}
				if(rr==-1) {
					System.err.println("EOF???");
					endMarkerFound = true;
					break;
				}
				for(int i=0; i<rr; i++) {
					if(myBuffer[i]!='\0' && myBuffer[i]!='\n' && myBuffer[i]!='\r') {
						sb.append((char)myBuffer[i]);
//						System.out.println("Currently sb contains " + sb.toString() + " (length = " + sb.length() + ")");
					} else {
						applyCheck(sb.toString());
						sb.setLength(0);
					}
				}
			}
		}
	}

	
	void doAll(String[] args) {
		System.out.println("args[0] = " + args[0]);
		long timeZero = System.currentTimeMillis();
		PipedOutputStream pipedOut = new PipedOutputStream();
		PipedInputStream pipedIn;
		try {
			pipedIn = new PipedInputStream(pipedOut);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		Thread genner = new Thread(new GenAllPaths(pipedOut));
		genner.start();
		Thread chooser = new Thread(new ChooseResults(pipedIn, System.out, args[0], genner));
		chooser.start();
		long timeOOO = System.currentTimeMillis();
		try {
			genner.join();
			pipedOut.close();
			System.out.println("pipedOut closed successfully");
		} catch (InterruptedException e) {
			System.err.println("Couldn't join genner");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Couldn't close pipedOut");
			e.printStackTrace();
		}
		long timeOne = System.currentTimeMillis();
		try {
			chooser.join();
			pipedIn.close();
			System.out.println("pipedIn closed successfully");
		} catch (InterruptedException e) {
			System.err.println("Couldn't join chooser");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Couldn't close pipedIn");
			e.printStackTrace();
		}
		long timeTwo = System.currentTimeMillis();
		System.out.println("timeOOO = " + (timeOOO - timeZero));
		System.out.println("timeOne = " + (timeOne - timeZero));
		System.out.println("timeTwo = " + (timeTwo - timeZero));
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("args[0] = " + args[0]);
		new SearchWithPipe().doAll(args);
	}

}
  