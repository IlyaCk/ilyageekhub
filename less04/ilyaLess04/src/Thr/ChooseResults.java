package Thr;

import java.util.ArrayList;
import java.util.List;

class ChooseResults implements Runnable {
	static long theMatchNumber = 0L;
	List<String> listToLook;
	Thread threadToJoin;
	String pattAsRegExp;
	
	public ChooseResults(List<String> toLook, String pattRE, Thread toJoin) {
		listToLook = toLook;
		threadToJoin = toJoin;
		pattAsRegExp = pattRE;
	}
	

	@Override
	public void run() {
		List<String> res = new ArrayList<>();
		for(String currFileName : listToLook) {
			if(currFileName.matches(pattAsRegExp)) {
				theMatchNumber++;
				res.add("match " + theMatchNumber + " : " + currFileName);
			} 
		}
		if(threadToJoin != null) {
			try {
				threadToJoin.join();
			} catch (InterruptedException e) {
				System.err.println("Couldn't join");
				System.err.println(e.getMessage());
			}
		}
		for(String chosen : res) {
			System.out.println(chosen);
		}
	}
}
