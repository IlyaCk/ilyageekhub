package Thr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchThreadEach0x4000 {
	String pattAsRegExp;
	List<String> allFileNames;
	ArrayList<Thread> usedChoosers;

	void searchFrom(File dir) {
		String[] lstAll = dir.list();
		if(lstAll == null) {
			return;
		}
		for(String fname : lstAll) {
			File fnext = new File(dir, fname);
			if(fnext.isDirectory()) {
				searchFrom(fnext);
			} else {
				try {
					allFileNames.add(fnext.getCanonicalPath());
				} catch (IOException e) {
					System.err.println("exception in pusing to list " + fname + " inside " + dir.getAbsolutePath());
					e.printStackTrace();
				}
			}
		}
		if(allFileNames.size() >= 0x4000) {
			Thread nextChooser = new Thread(new ChooseResults(allFileNames, pattAsRegExp, usedChoosers.get(usedChoosers.size()-1)));
			usedChoosers.add(nextChooser);
			nextChooser.start();
			System.err.println("new chooser started due to list size = " + allFileNames.size());
			allFileNames = new ArrayList<>();
		}
	}

	public void doAll(String[] args) {
		if(args.length < 1) {
			System.err.println("Usage : " + this.getClass().getCanonicalName() + " <pattern to search>");
			return;
		}
		String patt = args[0];
		if(patt.contains(".")) {
			pattAsRegExp = ".*\\\\" + patt.replaceAll("\\.", "\\\\.").replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\.]*";
		} else {
			pattAsRegExp = ".*" + patt.replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\]*";
		}
		allFileNames = new ArrayList<>();
		usedChoosers = new ArrayList<>();
		System.out.println("patt = " + patt);
		System.out.println("pattAsRegExp = " + pattAsRegExp);
		long timeZero = System.currentTimeMillis();
		File startDir = new File(".");
		usedChoosers.add(null);
		searchFrom(startDir);
		Thread nextChooser = new Thread(new ChooseResults(allFileNames, pattAsRegExp, usedChoosers.get(usedChoosers.size()-1)));
		usedChoosers.add(nextChooser);
		nextChooser.start();
		System.err.println("new chooser started due to all directories already walked. List size = " + allFileNames.size());

		long timeOne = System.currentTimeMillis();
		System.out.println(timeOne - timeZero);
		if(!usedChoosers.isEmpty()) {
			try {			
				usedChoosers.get(usedChoosers.size()-1).join();
			} catch (InterruptedException e) {
				System.err.println("Could not join last chooser, in main class");
				System.err.println(e.getMessage());
			}
		}
		long timeTwo = System.currentTimeMillis();
		System.out.println("" + (timeTwo - timeZero) + " = " + (timeOne - timeZero) + "(gen)  + " + (timeTwo - timeOne) + "(select)");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		(new SearchThreadEach0x4000()).doAll(args);
	}

}
