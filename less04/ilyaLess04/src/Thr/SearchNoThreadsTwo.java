package Thr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchNoThreadsTwo {
	long theMatchNumber;
	String pattAsRegExp;
	
	List<String> allFileNames;

	void searchFrom(File dir) {
		String[] lstAll = dir.list();
		if(lstAll == null) {
			return;
		}
		for(String fname : lstAll) {
			File fnext = new File(dir, fname);
			if(fnext.isDirectory()) {
				searchFrom(fnext);
			} else {
				try {
					allFileNames.add(fnext.getCanonicalPath());
				} catch (IOException e) {
					System.err.println("exception in pusing to list " + fname + " inside " + dir.getAbsolutePath());
					e.printStackTrace();
				}
			}
		}
	}

	public void doAll(String[] args) {
		if(args.length < 1) {
			System.err.println("Usage : " + this.getClass().getCanonicalName() + " <pattern to search>");
			return;
		}
		String patt = args[0];
		theMatchNumber = 0;
		if(patt.contains(".")) {
			pattAsRegExp = ".*\\\\" + patt.replaceAll("\\.", "\\\\.").replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\.]*";
		} else {
			pattAsRegExp = ".*" + patt.replaceAll("\\*", "[^\\\\\\\\]*") + "[^\\\\]*";
		}
		this.allFileNames = new ArrayList<>();
		System.out.println("patt = " + patt);
		System.out.println("pattAsRegExp = " + pattAsRegExp);
		long timeZero = System.currentTimeMillis();
		File startDir = new File(".");
		searchFrom(startDir);
		long timeOne = System.currentTimeMillis();
		System.out.println(timeOne - timeZero);
		
		System.out.println("tmp list size = " + allFileNames.size());
		
		for(String currFileName : allFileNames) {
			if(currFileName.matches(pattAsRegExp)) {
				theMatchNumber++;
				System.out.println("match " + theMatchNumber + " : " + currFileName);
			} 
		}
		long timeTwo = System.currentTimeMillis();
		System.out.println("" + (timeTwo - timeZero) + " = " + (timeOne - timeZero) + "(gen)  + " + (timeTwo - timeOne) + "(select)");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		(new SearchNoThreadsTwo()).doAll(args);
	}

}
