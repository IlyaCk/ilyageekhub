package netw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;

class MyServer implements Runnable {
	int thePort;

	MyServer(int p) {
		thePort = p;
	}

	public void run() {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(thePort);
			System.out.println("waiting for create socket at port " + thePort);
			Socket socket = serverSocket.accept();
			System.out.println("socket has been created at port " + thePort);
			
			BufferedReader socketReader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			Writer socketWriter = new OutputStreamWriter(
					socket.getOutputStream());

//			InputStream is = socket.getInputStream();
//			OutputStream os = socket.getOutputStream();
//			int rr = 0;
//			byte[] myBuffer = new byte[0x1040];
//			StringBuilder sb = new StringBuilder();
			int numFails = 0;
			while (numFails < 20) {
				String strFromSocket = null;
				try {
					strFromSocket = socketReader.readLine();
					numFails = 0;
				} catch (IOException e) {
					numFails++;
					System.err.println("Couldn't read from socket, waiting");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException eee) {
						break;
					}
					continue;
				}
				if(strFromSocket == null || strFromSocket.isEmpty()) {
					continue;
				}
				System.out.println("socket >> " + strFromSocket);
				String strSelected = strFromSocket.substring(0, 1); 
				System.out.println(strSelected + " >> socket");
				socketWriter.write(strSelected);
//				socketWriter.newLine();
//				socketWriter.flush();
/*				
				try {
					rr = is.read(myBuffer, 0, 0x1000);
				} catch (NullPointerException | IndexOutOfBoundsException e) {
					System.err.println("Incorrect work of socket's read");
					e.printStackTrace();
					rr = -1;
				} catch (IOException e) {
					System.err.println("Can't read from socket");
					try {
						Thread.sleep(5000);
						System.err.println("sleep really applied");
						rr = is.read(myBuffer);
						System.err.println("After pause, could read from socket");
					} catch (IOException | InterruptedException eee) {
						System.err.println("Finally Can't read from socket");
						rr = -1;
						break;
					}
				}
				if (rr == -1) {
					System.err.println("EOF???");
					break;
				}
				for (int i = 0; i < rr; i++) {
					if (myBuffer[i] != '\0' && myBuffer[i] != '\n'
							&& myBuffer[i] != '\r') {
						sb.append((char) myBuffer[i]);
					} else {
						System.out.println("socket >> " + sb.toString());
						System.out.println(sb.substring(0, 1) + " >> socket");
						os.write((sb.substring(0, 1) + "\n").getBytes());
						sb.setLength(0);
					}
				}
*/				
			}
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (serverSocket != null) {
					serverSocket.close();
					System.out.println("socket was successfully closed");
				}
			} catch (IOException e) {
				System.err.println("Can't close socket");
				e.printStackTrace();
			}
		}
	}
}

public class ServerSocketTrier {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Thread[] allServers = new Thread[100];
		for (int i = 0; i < 100; i++) {
			allServers[i] = new Thread(new MyServer(i + 17100));
			allServers[i].start();
		}
	}
}
