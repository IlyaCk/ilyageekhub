package netw;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class ClientSocketTrier {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Socket socket = null;
		StringBuilder sb = new StringBuilder();
		try {
			socket = new Socket("localhost", 17117);
			OutputStream sockOut = socket.getOutputStream();
			InputStream sockIn = socket.getInputStream();
			int tick=0;
			while(true) {
				System.out.println("" + tick + " >> socket"); 
				sockOut.write(("" + tick + "\n").getBytes());

				int rr = 0;
				byte[] myBuffer = new byte[0x1000];
				try {
					rr = sockIn.read(myBuffer , 0, 0x1000);
				} catch (NullPointerException | IndexOutOfBoundsException e) {
					System.err.println("Incorrect work of socket's read");
					e.printStackTrace();
					rr = -1;
				} catch (IOException e) {
					System.err.println("Can't read from socket");
					try {
						Thread.sleep(5000);
						System.err.println("sleep really applied");
						rr = sockIn.read(myBuffer);
						System.err.println("After pause, could read from socket");
					} catch (IOException | InterruptedException eee) {
						System.err.println("Finally Can't read from socket");
						rr = -1;
						break;
					}
				}
				if(rr==-1) {
					System.err.println("EOF???");
					break;
				}
				for(int i=0; i<rr; i++) {
					if(myBuffer[i]!='\0' && myBuffer[i]!='\n' && myBuffer[i]!='\r') {
						sb.append((char)myBuffer[i]);
					} else if(!sb.toString().isEmpty()) {
						System.out.println("socket >> " + sb.toString());
						System.out.println(sb.substring(0, 1) + " >> socket");							
						sb.setLength(0);
					}
				}
				
				
				TimeUnit.SECONDS.sleep(2);
				tick++;
			}
		} catch (IOException | InterruptedException e) {
			System.err.println(e.getMessage());
		} finally {
			if(socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					System.err.println("failed to close");
				}
			}
		}
	}

}
