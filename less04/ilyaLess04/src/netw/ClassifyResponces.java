package netw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClassifyResponces {
	
	private static final int NO_RESPONSE = 666;

	static void printMap(Map<String,Integer> m, String head)
	{
		if(m.isEmpty()) {
			System.out.println("\nThere were no links treated as ``" + head + "''\n");
		} else {
			System.out.println("\nStarting list of links treated as ``" + head + "''");
			System.out.println("v v v v v v v v v v v v v v v v v v v v v v v v v v v");
			for(Entry<String, Integer> en : m.entrySet()) {
				System.out.println(en.getValue() + "\t" + en.getKey());
			}
			System.out.println("^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^");
			System.out.println("Finishing list of links treated as ``" + head + "''\n");
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			URL myURL = new URL("http://intertelecom.ua/ua");
			BufferedReader in = new BufferedReader(new InputStreamReader(myURL.openStream()));
			System.out.println(in);
			String str;
			StringBuilder allPageTextBuilder = new StringBuilder();
			while((str = in.readLine()) != null) {
				allPageTextBuilder.append(str + " ");
			}
			in.close();
			String allPageText = allPageTextBuilder.toString();
			Pattern p1 = Pattern.compile("<\\s*(a|link)[^<>]*href\\s*=\\s*(\"[A-Za-z0-9\\-_\\.~!\\*'\\(\\);:@&=\\+$,/\\?%#\\[\\]]*\")");
			Matcher m = p1.matcher(allPageText); 
			System.out.println("Matcher " + m + " is preparing to start");
			
			String myURLprefix = myURL.getPath();
			myURLprefix.replaceFirst("([^/])/[^/].*", "$1");
			System.out.println(myURLprefix);
			Map<String, Integer> knownResponces = new TreeMap<>();
			while(m.find()) {
				String currLinkAll = m.group();
				System.out.println(currLinkAll);
				String currLinkSelected = currLinkAll.replaceAll("<\\s*(a|link)[^<>]*href\\s*=\\s*\"([A-Za-z0-9\\-_\\.~!\\*'\\(\\);:@&=\\+$,/\\?%#\\[\\]]*)\"", "$2");
				System.out.println(currLinkSelected);
				/*
				if(!(currLinkSelected.contains("//") || currLinkSelected.contains(":"))) {
					if(currLinkSelected.charAt(0)=='/') {
						currLinkSelected = myURLprefix + currLinkSelected;
					} else {
						currLinkSelected = myURL.getPath() + currLinkSelected;
					}
					System.out.println("(actually          " + currLinkSelected + ")");
				}
				*/
				if(!knownResponces.containsKey(currLinkSelected)) {
					try {
						//	URL linkedURL = new URL(currLinkSelected);
						URL linkedURL = new URL(myURL, currLinkSelected);
						URLConnection conn = linkedURL.openConnection();
						if(conn instanceof HttpURLConnection) {
							((HttpURLConnection) conn).setRequestMethod("HEAD");
							int respCode = ((HttpURLConnection) conn).getResponseCode();
							knownResponces.put(currLinkSelected, respCode);
						} else {
							knownResponces.put(currLinkSelected, 999);
						}
					} catch(IOException e) {
						knownResponces.put(currLinkSelected, NO_RESPONSE);
						System.err.println(currLinkSelected + " -- no responce at all");
					}
				}
			}
			System.out.println("Matcher finished");
			Map<String,Integer> nonHTTP = new TreeMap<>();
			Map<String,Integer> working = new TreeMap<>();
			Map<String,Integer> redirected = new TreeMap<>();
			Map<String,Integer> nonWorking = new TreeMap<>();
			Map<String,Integer> noRespAtAll = new TreeMap<>();
			for(Entry<String,Integer> en : knownResponces.entrySet()) {
				if(en.getValue() == 999) {
					nonHTTP.put(en.getKey(), en.getValue());
				} else if(en.getValue() == 666) {
					noRespAtAll.put(en.getKey(), en.getValue());
				} else if(en.getValue() >= 400) {
					nonWorking.put(en.getKey(), en.getValue());
				} else if(en.getValue() >= 300) {
					redirected.put(en.getKey(), en.getValue());
				} else if(en.getValue() >= 200) {
					working.put(en.getKey(), en.getValue());
				} else {
					System.err.println("Strange responce code");
				}
			}
			printMap(noRespAtAll, "NO RESPONCE");
			printMap(nonHTTP, "non-HTTP");
			printMap(nonWorking, "non-working");
			printMap(redirected, "redirected");
			printMap(working, "working looks like OK");
			
			
		} catch (UnknownHostException e) {
			System.err.println("Trying to connect to unknown host, quitting");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
