package ilyaLess01;



public class FibonacciSequence {

	/**
	 * finds and prints Fibonacci sequence,
	 * F[1] = 1, F[2] = 1,
	 * forall i > 2, F[i] = F[i-1] + F[i-2]
	 */
	public static void main(String[] args) {
		try {
			int N = new Integer(args[0]);
			if(N>=1) {
				System.out.println("Fib[1] = " + 1);
				if(N>=2) {
					System.out.println("Fib[2] = " + 1);
				}
			}
			long fPrevPrev = 1, fPrev = 1;
			long fCurr;
			for(int j=3; j<=N; j++) {
				fCurr = fPrev + fPrevPrev;
				System.out.println("Fib[" + j + "] = " + fCurr);
				fPrevPrev = fPrev;
				fPrev = fCurr;
			}
		} catch(ArrayIndexOutOfBoundsException e) {
			System.err.println("You should pass N as cmd-line parameter");
		}  catch(NumberFormatException e) {
			System.err.println("Cmd-line parameter should be decimal number");
		}

	}

}
   