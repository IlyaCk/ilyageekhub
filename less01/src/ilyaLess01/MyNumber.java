package ilyaLess01;

public class MyNumber {
	private Long asInteger;
	private Double asDouble;
	private boolean isInteger;
	private byte radixIfInteger;

	/**
	 * 
	 * @param String s
	 * @throws NumberFormatException
	 * Constructs MyNum from String presentation.
	 * For integers, use 
	 * 		0... for octal,
	 * 		0x... for hexadecimal,
	 * 		... for decimal.
	 * For floating-point (doubles), only decimal is allowed
	 *   
	 */
	public MyNumber(String s) throws NumberFormatException {
		try {
			if(s.contains(".") 
					||  s.length()>1 && Character.toUpperCase(s.charAt(1))!='X' && (s.contains("e") || s.contains("E"))) {
				isInteger = false;
				radixIfInteger = -1;
				asInteger = null;
				asDouble = new Double(s); 
			} else {
				asDouble = null;
				isInteger = true;
				if(s.charAt(0)=='0') {
					if(s.length() > 1) {
						if(Character.toUpperCase(s.charAt(1))=='X') {
							radixIfInteger = 16;
							asInteger = Long.parseLong(s.substring(2), 16);
						} else {
							radixIfInteger = 8;
							asInteger = Long.parseLong(s.substring(1), 8);
						}	
					} else {
						radixIfInteger = 10;
						asInteger = 0L;
					}  
				} else {
					radixIfInteger = 10;
					asInteger = Long.parseLong(s);
				}
			}
		} catch(NumberFormatException e) {
			isInteger = false;
			radixIfInteger = -1;
			asInteger = null;
			asDouble = null;
			throw new NumberFormatException();
		}
	}
	
	private MyNumber(double value) {
		isInteger = false;
		radixIfInteger = -1;
		asInteger = null;
		asDouble = new Double(value);
	}
	
	private MyNumber(long value, byte radix) {
		isInteger = true;
		radixIfInteger = radix;
		asInteger = new Long(value);
		asDouble = null;
	}
	
	/**
	 * performs addition:
	 * when at least one number is non-integer, result is non-integer;
	 * when numbers are both integer and in the same base, 
	 * 		result's radix is the same;
	 * when numbers are both integer but in different bases, 
	 * 		decimal system is used as default
	 */
	public MyNumber add(MyNumber that) {
		if(!this.isInteger || !that.isInteger) {
			double value1 = this.isInteger ? new Double(this.asInteger.toString()) : this.asDouble;
			double value2 = that.isInteger ? new Double(that.asInteger.toString()) : that.asDouble;
			return new MyNumber(value1 + value2);		
		}
		return new MyNumber(this.asInteger + that.asInteger, 
				(this.radixIfInteger == that.radixIfInteger	
						? this.radixIfInteger
						: 10));
	}
	
	public String toString() {
		if(isInteger) {
			return (radixIfInteger == 16 ? "0x" : (radixIfInteger == 8 ? "0" : ""))
					+ Long.toString(asInteger, radixIfInteger);
		} else { // double
			return asDouble.toString();				
		}
	}
}
   