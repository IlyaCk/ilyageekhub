package ilyaLess01;

public class CharSequence {

	/**
	 * prints all chars with codes in range [ m .. n ]
	 */
	public static void main(String[] args) {
		try{
			int m = new Integer(args[0]);
			int n = new Integer(args[1]);
			char startChar = (char)m;
			char lastChar = (char)n;
			if(m>n) {
				System.out.println("WARNING: m>n, chars are in descending order");
				for(char currChar = startChar; currChar >= lastChar; currChar--) {
					System.out.print(currChar);
				}
			} else {
				for(char currChar = startChar; currChar <= lastChar; currChar++) {
					System.out.print(currChar); 
				}
			}
		} catch(ArrayIndexOutOfBoundsException e) {
			System.err.println("You should pass m and n as cmd-line parameters");
		} catch(NumberFormatException e) {
			System.err.println("Cmd-line parameters should be natural decimal numbers");
		}
		

	}

}
   