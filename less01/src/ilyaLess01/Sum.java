package ilyaLess01;

public class Sum {

	public void mySolve(String[] args) {
		try {
			MyNumber num1 = new MyNumber(args[0]);
			MyNumber num2 = new MyNumber(args[1]);
			System.out.println(num1.add(num2).toString());
		} catch(ArrayIndexOutOfBoundsException e) {
			System.err.println("You should pass m and n as cmd-line parameters");
		} catch(NumberFormatException e) {
			System.err.println("Could not determine number's format");
		}
	}

	
	/**
	 * calculates sum of two numbers
	 */
	public static void main(String[] args) {
		new Sum().mySolve(args);
	}

}
