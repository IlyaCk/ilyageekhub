package ilyaLess01;


public class FractionSequence {

	/**
	 * finds and prints sequence of 1/1, 1/2, 1/3, ..., 1/n
	 */
	public static void main(String[] args) {
		try{
			int N = new Integer(args[0]);
			if(N<=0) {
				throw new NumberFormatException();
			}
			for(int j=1; j<=N; j++) {
				double theFrac = 1.0 / j;
				System.out.println("1 / " + j + " = " + theFrac);
			}
		} catch(ArrayIndexOutOfBoundsException e) {
			System.err.println("You should pass N as cmd-line parameter");
		} catch(NumberFormatException e) {
			System.err.println("Cmd-line parameter should be natural decimal number");
		}

	}

}
