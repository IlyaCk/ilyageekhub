package ilyaLess05;

import ilyaLess01.MyNumber;

//import ilyaLess02.*; 

public class BeanRepresenterLauncher {
	
	Object createWhomToRepresent() {
/*		
		int[] c = {5,5,5};
		Cat murzikCat = new CatOrthodoxEquals(c, 1);
		return murzikCat;
*/
/*		
		VoteBox theVB1 = new VoteBox("A", "B");
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("C"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("D"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("C"));
		theVB1.add(new Vote("A"));
		theVB1.add(new Vote("B"));
		theVB1.add(new Vote("A"));
		return theVB1;
*/
/*		
		ArrayList<MyNumber> lst = new ArrayList<>();
		lst.add(new MyNumber("0x0"));
		lst.add(new MyNumber("00"));
		lst.add(new MyNumber("0"));
		lst.add(new MyNumber("0.0"));
*/
		MyNumber[][] lst = {
				{new MyNumber("0x0"), new MyNumber("0"), new MyNumber("00"), new MyNumber("00"), new MyNumber("0.0"), new MyNumber("0x12"), new MyNumber("012"), new MyNumber("12")},
				new MyNumber[7]
		};
		lst[1][0] = new MyNumber("0x12").add(new MyNumber("0.12"));
		lst[1][1] = lst[0][2];
		lst[1][3] = new MyNumber("0x12").add(new MyNumber("0.12"));
		lst[1][4] = new MyNumber("18");
		lst[1][5] = new MyNumber("0xDEADBEEF");
		lst[1][6] = new MyNumber("0xDEADBEEF");
		return lst;
	}
	
	void doAll() {
		Object ob = createWhomToRepresent(); 
		BeanRepresenter repr = new BeanRepresenter(ob, System.out);
		repr.doRepresent();
//		BeanRepresenter reprSecond = new BeanRepresenter(repr, System.out);
//		reprSecond.doRepresent();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BeanRepresenterLauncher brl = new BeanRepresenterLauncher();
		brl.doAll();
	}
}
///