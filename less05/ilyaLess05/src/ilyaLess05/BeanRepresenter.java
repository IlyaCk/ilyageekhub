package ilyaLess05;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BeanRepresenter {
	class Pair<TypeOne, TypeTwo> {
		protected TypeOne value1;
		protected TypeTwo value2;
		public Pair(TypeOne v1, TypeTwo v2) {
			value1 = v1;
			value2 = v2;
		}
		public boolean valueOneEqualByPointer(TypeOne v1) {
			return value1 == v1; 
		}
	}
	
	Integer contain(List<Pair<Object, Integer>> lst, Object ob) {
		for(Pair<Object,Integer> p : lst) {
			if(p.valueOneEqualByPointer(ob)) {
				return p.value2;
			}
		}
		return null;
	}
	
	Object rootObject;
	PrintStream outToUse;
	int linesUsed;
	List<Pair<Object, Integer>> alreadyPrinted;
	public BeanRepresenter(Object ob, PrintStream out) {
		rootObject = ob;
		outToUse = out;
		linesUsed = 0;
		alreadyPrinted = new ArrayList<>();
	}
	
	protected String currObjRepresent(Object ob, String obName) {
		if(ob == null) {
			return "<null>";
		} 
		String allObjToStr = (ob.getClass().isArray()) ? Arrays.deepToString((Object[])ob) : ob.toString();
		if(allObjToStr.length() > 60) {
			allObjToStr = allObjToStr.substring(0, 48) + " ... (totally " + allObjToStr.length() + " chars)";
		}
		allObjToStr = allObjToStr.replaceAll("\\n", "\\\\n");
		return ob.getClass().toString() + "\t:\t" + obName + "\t:\t" + allObjToStr;
	}

	protected void reprRec(Object curr, String obName, boolean[] isLast) {
		linesUsed++;
		outToUse.printf("\nline%5d : ", linesUsed);
		StringBuilder prefix = new StringBuilder();
		for(int i=0; i+1 < isLast.length; i++) {
			prefix.append((isLast[i]) ? "  " : "| ");
		}
		prefix.append(isLast[isLast.length - 1] ? "\\ " : "+ ");
		outToUse.print(prefix + currObjRepresent(curr, obName));
		Integer whereWasBefore = contain(alreadyPrinted, curr);
		if(whereWasBefore != null) {
			outToUse.print(" ----- this object was already represented at line #" + whereWasBefore);
			return;
		}
		if(curr == null || curr instanceof Number || curr instanceof Boolean || curr instanceof Locale) {
			return;
		}
		alreadyPrinted.add(new Pair<Object,Integer>(curr, linesUsed));
		if(curr.getClass().isArray())
		{
			int len = ((Object[])curr).length;
			if(len > 0) {
				boolean[] isLastCopy = new boolean[isLast.length + 1];
				for(int i=0; i<isLast.length; i++) {
					isLastCopy[i] = isLast[i];
				}
				for(int idx=0; idx < len; idx++) {
					isLastCopy[isLast.length] = (idx+1 == len);
					reprRec(((Object[])curr)[idx], obName + "[" + idx + "]", isLastCopy);
				}
			}
			return;		
		}
		try {
			Map<String, Field> allFieldsMap = new TreeMap<>();
			Field[] currHierarchyLevelFields = curr.getClass().getDeclaredFields();
			Field[] allAncestorsFields = curr.getClass().getFields();
			if(allAncestorsFields != null) {
				for(Field currFld : allAncestorsFields) {
					allFieldsMap.put(currFld.getName(), currFld);
				}
			}
			if(currHierarchyLevelFields != null) {
				for(Field currFld : currHierarchyLevelFields) {
					allFieldsMap.put(currFld.getName(), currFld);
				}
			}
			Field[] allFields = new Field[allFieldsMap.size()];
			int fldIdx = 0;
			for(Field f : allFieldsMap.values()) {
				allFields[fldIdx++] = f;
			}
			boolean[] isLastCopy = new boolean[isLast.length + 1];
			for(int i=0; i<isLast.length; i++) {
				isLastCopy[i] = isLast[i];
			}
			for(int fldCnt = 0; fldCnt < allFields.length; fldCnt++) { 
				if(!allFields[fldCnt].getClass().isAnnotation() && !allFields[fldCnt].getClass().isPrimitive()) {
					isLastCopy[isLast.length]  =  fldCnt+1 == allFields.length;
					try {
						allFields[fldCnt].setAccessible(true);
						reprRec(allFields[fldCnt].get(curr), allFields[fldCnt].getName(), isLastCopy);
					} catch (IllegalArgumentException e) {
						System.err.println("illegal argument while accessing field #" + fldCnt);
					} catch (IllegalAccessException e) {
						System.err.println("cannot access field #" + fldCnt);
					}
				}
			}
		} catch (SecurityException e) {
			outToUse.print("  <-- cannot go deeper due to SecurityException");
		}
	}
	
	public void doRepresent() {
		boolean[] isLast = new boolean[1];
		isLast[0] = true;
		reprRec(rootObject, "<whole passed instance>", isLast);
	}

}
///